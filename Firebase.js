import "firebase/auth";
import "firebase/firestore";
import firebase from "firebase/app";
import { authState } from "rxfire/auth";
import { collectionData } from "rxfire/firestore";
import { filter } from "rxjs/operators";
const app = firebase.initializeApp({
  apiKey: "AIzaSyAKsclSnf_Ab29iDdV8JB7d6PdKVN3qVdk",
  authDomain: "moviepals-2f98c.firebaseapp.com",
  databaseURL: "https://moviepals-2f98c.firebaseio.com",
  projectId: "moviepals-2f98c",
  storageBucket: "moviepals-2f98c.appspot.com",
  messagingSenderId: "536679623503",
  appId: "1:536679623503:web:bae7c32b997cd2adef733a",
  measurementId: "G-V1QSC7SXEH"
});
const firestore = firebase.firestore(app); // Initialize firestore
const auth = firebase.auth(app); // Initialize firebase auth 
const loggedIn$ = authState(auth).pipe(filter(user => !!user)); // Observable only return when user is logged in. 
export { app, auth, firestore, collectionData, loggedIn$ }; 
export default firebase;
